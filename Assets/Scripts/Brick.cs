﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

    
    public Sprite[] hitSprites;

    public static int breakableCount = 0;

    private int maxHits;
    private int timesHit;
    private LevelManager levelManager;
    private bool isBreakable;

    
    
    // Use this for initialization
	void Start ()
    {
        SetInitialReferences();

        if (isBreakable) breakableCount++;
        print(breakableCount);
    }

    private void SetInitialReferences()
    {
        isBreakable = (this.tag == "Breakable");
        maxHits = hitSprites.Length + 1;
        levelManager = FindObjectOfType<LevelManager>();
        timesHit = 0;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (isBreakable) HandleHits();

    }

    private void HandleHits()
    {
        timesHit++;
        if (timesHit >= maxHits)
        {
            breakableCount--;
            print(breakableCount);
            levelManager.BrickDestroyed();
            Destroy(gameObject);
            
            
        }
        else
        {
            LoadSprites();
        }
    }

    void LoadSprites()
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex])
        {
            this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
    }

    void SimulateWinning()
    {
        levelManager.LoadNextLevel();
    }


}
