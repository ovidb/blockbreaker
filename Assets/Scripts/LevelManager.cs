﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name){
        Brick.breakableCount = 0;
        SceneManager.LoadScene (name);
        
	}

	public void QuitRequest(){
		Debug.Log ("Quit requested");
		Application.Quit ();
	}

    public void LoadNextLevel()
    {
        Brick.breakableCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void BrickDestroyed()
    {
        print("brickDestryed called");
        if (Brick.breakableCount <= 0)
        {
            LoadNextLevel();
        }
        else
        {
            print("Blocks remaining: " + Brick.breakableCount);
        }
    }
}
