﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    
    public float velocityX;
    public float velocityY;
    private Rigidbody2D rb;

    [SerializeField] private Paddle paddle;
    private Vector3 paddleToBallVector;
    private bool hasStarted = false;
	
    // Use this for initialization
	void Start () {
        SetInitialReferences();
	}

    void SetInitialReferences()
    {
        paddle = FindObjectOfType<Paddle>();
        rb = GetComponent<Rigidbody2D>();
        paddleToBallVector = this.transform.position - paddle.transform.position;
        // print(paddleToBallVector.y);
    }

    // Update is called once per frame
    void Update () {
        if (!hasStarted) { 
            transform.position = paddle.transform.position + paddleToBallVector;
            if (Input.GetMouseButtonDown(0))
            {
                hasStarted = true;
                rb.velocity = new Vector2(velocityX, velocityY);
            }
        }
    }
}
