﻿using UnityEngine;
using System.Collections;

public class LoseColider : MonoBehaviour
{
    private LevelManager levelManager;

    void Start()
    {
        SetInitialReferences();
    }

    void SetInitialReferences()
    {
        levelManager = FindObjectOfType<LevelManager>();

    }

    void OnTriggerEnter2D(Collider2D triggeredBy)
    {
        Debug.Log(gameObject.name + " has been triggered by " + triggeredBy.name);
        //TODO: Should die here.
        levelManager.LoadLevel("Lose Screen");
    }
    
    void OnCollsionEnter2D(Collision2D colidedWith)
    {
        Debug.Log(gameObject.name + " colliding with " + colidedWith.gameObject.name);
    }
}
